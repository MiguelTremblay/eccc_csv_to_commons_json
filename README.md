Convert MSC Weather Observations in CSV format to Wikimedia Commons JSON format
=============

Introduction
------------

`msc_csv_to_commons_json.py` is a python3 script used to convert [observation files from Meteorological Service of Canada](http://climate.weather.gc.ca/historical_data/search_historic_data_e.html) (MSC) CSV format to JSON format to be incorporated into [Wikimedia Commons Structured Data](https://commons.wikimedia.org/wiki/Commons:Structured_data).

This script can be used in combination with [Get Canadian Weather Observations script](https://framagit.org/MiguelTremblay/get_canadian_weather_observations) allowing to download MSC weather observation in your local computer.

This software works under GNU/Linux, Windows and Mac OS X.
___

Requirements
------------

* [python3](https://www.python.org/downloads/)
* [python3-progress](https://pypi.python.org/pypi/progress)

___

Download
--------
The latest package can be downloaded here:<br>
https://framagit.org/MiguelTremblay/msc_csv_to_commons_json

The git version can be accessed here:<br>
 ```git clone https://framagit.org/MiguelTremblay/msc_csv_to_commons_json.git```


Manual
--------

In a general way, this application should be called in command line like this:
```bash
python msc_csv_to_commons_json.py [OPTIONS] INPUT
```
<br />
where:
* OPTIONS are described in the table below.
* INPUT is one or many local path to CSV files to be converted


| Options                                  | Description |
| -------                                  | ------------|
| `-h`, `--help`                           | Show help message and exit.|
| `-o` `--output-directory`&nbsp;DIRECTORY | Directory where the json files will be copied. Default value is where the input file is located.|
| `-N` `--no-clobber`                      | Do not overwrite an existing file.|
|`-t`  `--dry-run`                         | Execute the program, print the json content in the standard output, but do not write the file(s).|
| `-1` `--one-line  `                      | Write only one value per line (standard json layout). Default is all data values in one line.|
|`-v` `--verbose`                          | Explain what is being done.|
|`-V` `--version`                          | Output version information and exit.|

Usage
-----


```bash
 python msc_csv_to_commons_json.py /home/miguel/bagotville/monthly/eng-monthly-011942-122014.csv 
```
<br />

